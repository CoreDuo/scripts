#!/usr/local/bin/python

# RFC 2136-compliant Dynamic DNS updater
# Requirements: netifaces, dnspython
# pfSense: python, py27-netifaces, py27-dnspython

import netifaces as ni
import dns.query, dns.tsigkeyring, dns.update

# Retrieve the public IP address
interface = ni.ifaddresses('xl0') # WAN Interface
ip = interface[ni.AF_INET][0]['addr']

# Retrieve TSIG key
# dnssec-keygen -a hmac-sha512 -b 512 -n HOST <key-name>
with open('/etc/ddns.key') as file:
    keydata=file.read().replace('\n', '')
keyring = dns.tsigkeyring.from_text({
    '<key-name>' : keydata
})

# Update DNS record with new IP address
update = dns.update.Update('<dns-zone>', keyring=keyring, keyalgorithm="hmac-sha512") # Ex: 'contoso.com'
update.replace('<record-to-change>', 60, 'a', ip) # Ex: 'example' would update example.contoso.com
response = dns.query.tcp(update, '<dns-master-server>')

