#!/usr/bin/env python

# Configurator for the proxy DHCP mode in Dnsmasq
# This will only work if FOG Project has been installed.
# It is supported on Ubuntu 12.04 LTS and higher.
# It may work on Debian, but this has not been tested.

import imp
import subprocess as sp
import sys
import os
import pwd

devnull = open('/dev/null', 'w')
pyversion = sys.version_info[0]

def check_root():
    cuser = pwd.getpwuid(os.getuid())[0]
    if cuser != 'root':
        print("\033[0;31mMust be run as root\033[0m")
        sys.exit()

def python_setup():
    print("\033[0;32mInstalling pip...\033[0m")
    if pyversion == 3:
        sp.check_call(["apt-get", "install", "-y", "python3-pip"],stdout=devnull)
    else:
        sp.check_call(["apt-get", "install", "-y", "python-pip"],stdout=devnull)
    if pyversion == 2:
        print("\033[0;32mInstalling python-dev...\033[0m")
        sp.check_call(["apt-get", "install", "-y", "python-dev"],stdout=devnull)

def module_setup():
    print("\033[0;32mChecking for Python modules...\033[0m")
    modules = set(["netifaces", "termcolor", "psutil"])
    for module in modules:
        try:
            imp.find_module(module)
        except ImportError:
            print("\033[0;32mInstalling " + module + " module...\033[0m")
            if pyversion == 3:
                sp.check_call(["pip3", "install", module],stdout=devnull)
            else:
                sp.check_call(["pip", "install", module],stdout=devnull)
    sp.check_call([sys.executable, "functions.py"])

def main():
    check_root()
    python_setup()
    module_setup()
main()
