#!/usr/bin/env python

import netifaces as ni
import subprocess as sp
import termcolor as tc
import psutil as pu
import sys

if sys.version_info[0] == 3:
    raw_input = input

devnull = open('/dev/null', 'w')

def install_dnsmasq():
    print(tc.colored("Installing dnsmasq...", "green"))
    sp.check_call(["apt-get", "install", "-y", "dnsmasq"], stdout=devnull)

def input_interface():
    while True:
        query = raw_input(tc.colored("The default interface is eth0. Would you like to change this? [Y/n] ", "yellow"))
        if query in ['y', 'Y', 'yes', 'Yes', 'YES']:
            iface = raw_input(tc.colored("What network interface would you like to use? ", "yellow"))
            return iface
        elif query in ['n', 'N', 'no', 'No', 'NO']:
            iface = "eth0"
            return iface
        else:
            print(tc.colored("Please enter Y or n. ", "red"))

def configure_dnsmasq(iface):
    print(tc.colored("Configuring dnsmasq...", "green"))
    interface = ni.ifaddresses(iface)
    ip = interface[ni.AF_INET][0]["addr"]
    with open("/etc/dnsmasq-proxydhcp.cfg", "a") as conf:
        conf.write("listen-address=" + ip + "\n")
        conf.write("port=0\n")
        conf.write("log-dhcp\n")
        conf.write("tftp-root=/tftpboot\n")
        conf.write("dhcp-boot=undionly.kpxe,," + ip + "\n")
        conf.write("dhcp-no-override\n")
        conf.write("pxe-prompt=\"Press F8 for boot menu\", 3\n")
        conf.write("pxe-service=X86PC, \"Boot from network\", undionly\n")
        conf.write("dhcp-range=" + ip + ",proxy\n")
    sp.check_call(["ln", "-s", "undionly.kpxe", "undionly.0"],stdout=devnull,cwd='/tftpboot')

def configure_init():
    pid = pu.Process(1)
    init = pid.name()
    if init == "systemd":
        print(tc.colored("Creating systemd unit for dnsmasq...", "green"))
        with open("/etc/systemd/system/proxydhcp.service", "a") as unit:
            unit.write("[Unit]\n")
            unit.write("Description=Dnsmasq in proxy DHCP mode\n\n")
            unit.write("[Service]\n")
            unit.write("ExecStart=/usr/sbin/dnsmasq --conf-file=/etc/dnsmasq-proxydhcp.cfg\n")
            unit.write("Type=forking\n\n")
            unit.write("[Install]\n")
            unit.write("WantedBy=multi-user.target")
        print(tc.colored("Starting dnsmasq...", "green"))
        sp.check_call(["systemctl","enable","proxydhcp"], stdout=devnull)
        sp.check_call(["systemctl","start","proxydhcp"], stdout=devnull)
    else:	
        print(tc.colored("Creating upstart job for dnsmasq...", "green"))
        with open("/etc/init/proxydhcp.conf", "a") as job:
            job.write("description \"Dnsmasq in proxy DHCP mode\"\n")
            job.write("start on runlevel [2345]\n")
            job.write("exec /usr/sbin/dnsmasq --conf-file=/etc/dnsmasq-proxydhcp.cfg")
        sp.check_call(["start", "proxydhcp"], stdout=devnull)
        print(tc.colored("Starting dnsmasq...", "green"))

def main():
    install_dnsmasq()
    iface = input_interface()
    configure_dnsmasq(iface)
    configure_init()
    print(tc.colored("Done!", "green"))
main()
